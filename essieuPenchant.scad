//essieu penchant V0.1
//essieu avant de tricycle penchant
//
//
//Cédric Doutriaux
//cdriko@free.fr
//licence creative commons
// http://creativecommons.org/licenses/by-sa/3.0/



include <RoueAvant.Sturmy.Archer.scad>;



//------------------------------------------------------------------------
//parametres
//-------------------------------------------------------------
//qualite de rendu (3-100 grand chiffre= temps de calcul très lent)
qualite=12;

//------------------------------paramètres géométriques (en mm):
SiegePedalier=800;//distance siege/moyeu de pedalier
hSiege=400;
voie=800;
empatement=voie*1.618;
hPedalier=450;
hTrapeze=450;//hauteur du trapeze de train avant
dzTrapeze=200;//decalage vertical du train avant
AngleDeChasse=12;
AnglePivot=18;
hMoyeu=40;
//410;//16 pouces
//540;//20pouces
diametreRoueAv=700;//16 pouces
diametreRoueAr=700;//20pouces
RPneuAv=15;//rayon pneu Av
RPneuAr=17;//rayon pneu Ar
LLame=250;//largueur d'une lame de suspension


//-----------------------------paramètres dynamiques
//22.5-45*sin(200*$t);//angle de gite anime
gite=10+20*sin(200*$t);;//angle de gite anime
//pour l'instant malfonctionnel
//ToDo :
//verifer angle torsion des lames d'essieu
//voir dans la fonction essieu();


//----------------------------------------------------valeurs déduites des précedentes
//trapeze de l'essieu avant
/*

A________lTrapeze________________________B
I                                        \
I hTrapeze                        \ HTrapeze
I                                           \
D__________LTrapeze__________________________C


*/
  

lTrapeze=2*(voie/2-((diametreRoueAv/2)+(hTrapeze/2)+dzTrapeze)*sin(AnglePivot));
LTrapeze=2*(voie/2-((diametreRoueAv/2)-(hTrapeze/2)+dzTrapeze)*sin(AnglePivot));
HTrapeze=(hTrapeze)*cos(AnglePivot);

//essieu

/*

E & D sont au sol
A & F sur le chassis


A________lTrapeze__________B
I                                \
I h                               \ H
I                                    \
F_____LTrapeze_______C
I                                      \
I                                       \
I                                        \
I                                          \
E___________voie/2_________D

*/
//longueurs
EA=((diametreRoueAv/2)+hTrapeze/2+dzTrapeze);
EF=((diametreRoueAv/2)-hTrapeze/2+dzTrapeze);
DB=(((dzTrapeze+(diametreRoueAv/2))*cos(gite+AnglePivot))+(HTrapeze/2));


//angles
FAB=-gite-asin(   (EA*(1-cos(gite)) )   /  (lTrapeze /2)    );
EFC=-gite-asin(   (EF*(1-cos(gite)) )   /(LTrapeze/2)     );
ABC=-gite-acos(   (DB*(1-cos(gite+AnglePivot)) )   /(lTrapeze/2)     );

//echo( ABC,"+",FAB);


///////////////////////////////////////////////////////////////////----------------------------------------------------------------



module trainAvant(){////////////////////////////////////////////////////////train avant







//bas
translate(v = [0,((diametreRoueAv/2)-HTrapeze/2+dzTrapeze)*sin(AngleDeChasse),-9-hTrapeze/2+dzTrapeze])
rotate(a=[0,EFC,0])
lame(LTrapeze);

//haut
translate(v = [0,((diametreRoueAv/2)+HTrapeze/2+dzTrapeze)*sin(AngleDeChasse),-9+hTrapeze/2+dzTrapeze])
rotate(a=[0,FAB,0])
lame(lTrapeze);





//1/2 trains
/*
translate(v = [0,((diametreRoueAv/2)+HTrapeze/2+dzTrapeze)*sin(AngleDeChasse),-9+hTrapeze/2+dzTrapeze])
rotate(a=[0,FAB,0])
translate(v = [lTrapeze/2,0,0])
rotate(a=[0,270-ABC,0])
translate(v = [0,0,-DB])
rotate(a=[-AngleDeChasse,-gite,180])
translate(v=[0,0,diametreRoueAv/2])
*/
translate(v = [0,((diametreRoueAv/2)+HTrapeze/2+dzTrapeze)*sin(AngleDeChasse),-9+hTrapeze/2+dzTrapeze])
rotate(a=[0,FAB,0])
translate(v = [-lTrapeze/2,0,0])
rotate(a=[-AngleDeChasse,-FAB+ABC+90,0])
DemiTrainAv();
//cylinder(h = 600, r=40,center=false, $fn = 3);

//DemiTrainAv();

/*
cylinder(h = 600, r=40,center=false, $fn = 3);

translate(v = [-voie/2,0,0])
rotate(a=[0,90+ABC,0])
translate(v=[0,diametreRoueAv/2,0])

*/

//
mirror([ 1, 0, 0 ])
translate(v = [0,((diametreRoueAv/2)+HTrapeze/2+dzTrapeze)*sin(AngleDeChasse),-9+hTrapeze/2+dzTrapeze])
rotate(a=[0,-FAB,0])
translate(v = [-lTrapeze/2,0,0])
rotate(a=[-AngleDeChasse,-260+FAB+ABC,0])
DemiTrainAv();









}///////////////////////////////////////////////////////////////////////////////////fin train avant

module tetedepivot(){
///calculs de position
translate(v = [0,((diametreRoueAv/2)+HTrapeze/2+dzTrapeze)*sin(AngleDeChasse),-9+hTrapeze/2+dzTrapeze])
rotate(a=[0,FAB,0])
translate(v = [lTrapeze/2,0,0])
rotate(a=[0,270-ABC,0])
translate(v = [0,0,-DB])
rotate(a=[-AngleDeChasse,-gite,180])
translate(v=[0,0,diametreRoueAv/2]);


}


module lame(largeur){//////////////////lame de suspension
//en bois lamellé collé

cube(size = [largeur,LLame,3], center = true);
translate(v = [0,0,-5])
rotate(a=[90,0,0])
cylinder(h = LLame, r=10,center=true, $fn = qualite);

}/////////////////////////////////////////////fin lame


//translate(v = [-30,120,-70])rotate(a=[0,0,-33])cube(size = [250,45,3], center = false);

module DemiTrainAv(){///////////////////////////////////////////////////demi-train
///attaché par le haut
//rotule haute
sphere(r=10,$fn=qualite,center = true);

translate(v = [0,0,-DB]){
//axe de rotation/direction
cylinder(h = DB, r=2,center=false, $fn = qualite);
//rotule basse
translate(v = [0,0,(DB-dzTrapeze-(HTrapeze/2))])
sphere(r=10,$fn=qualite,center = true);

//deplacement vers l'axe de rotation
rotate(a=[0,-AnglePivot,0])
translate(v = [0,0,diametreRoueAv/2]){
RoueAvantSturmyArcher();
translate(v = [hMoyeu/2,0,0])fusee();
}
//diametreRoueAv/2
}

	

}/////////////////////////////////////////////////////////////////////////////fin demi-train


module roue(){//////////////////////////////////roue

rotate(a=[0,90,0]){

//pneu
%rotate_extrude(convexity = 10, $fn = qualite)
translate([(diametreRoueAv/2)-(RPneuAv/2), 0, 0])
circle(r = RPneuAv, $fn = qualite);




}

}///////////////////////////fin  roue



////////////////////////////////////////////fusee
module fusee(){
epaisseurFusee=70;

rotate(a=[0,90,0]){difference() {
//flasque
cylinder(h = 6, r=40,center=true,$fn = qualite);
cylinder(h = 7, r=6,center=true,$fn = qualite);
}
}


translate(v = [0,0,8])linear_extrude(file = "fusee.dxf", height = 10, center = true);
translate(v = [0,0,0])linear_extrude(file = "fusee_interieur.dxf", height = 8, center = true);
translate(v = [0,0,-8])linear_extrude(file = "fusee.dxf", height = 10, center = true);

rotate(a=[90,0,0])translate(v = [0,0,0])linear_extrude(file = "fusee_interieur.dxf", height = 8, center = true);





}//////////////////////////////////////////////////////////////fin fusee
