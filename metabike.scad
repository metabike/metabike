//METABIKE
//générateur paramétrique de tricycles et vélos couchés.
//
//
//Cédric Doutriaux
//cdriko@free.fr
//licence creative commons
// http://creativecommons.org/licenses/by-sa/3.0/
//------------------------------------------------------------------------

include <essieuPenchant.scad>;


//parametres
//-------------------------------------------------------------
//qualite de rendu (3-100 grand chiffre= temps de calcul très lent)
qualite=12;

//------------------------------paramètres géométriques (en mm):
SiegePedalier=800;//distance siege/moyeu de pedalier
hSiege=400;
voie=800;
empatement=voie*1.618;
hPedalier=450;
hTrapeze=100;//hauteur du trapeze de train avant
dzTrapeze=150;//decalage vertical du train avant
AngleDeChasse=12;
AnglePivot=28;
hMoyeu=40;
//410;//16 pouces
//540;//20pouces
diametreRoueAv=540;//16 pouces
diametreRoueAr=540;//20pouces
RPneuAv=15;//rayon pneu Av
RPneuAr=17;//rayon pneu Ar
LLame=70;//largueur d'une lame de suspension


//-----------------------------paramètres dynamiques
//22.5-45*sin(200*$t);//angle de gite anime
gite=0;//angle de gite anime
//pour l'instant malfonctionnel
//ToDo :
//verifer angle torsion des lames d'essieu
//voir dans la fonction essieu();

cambrure=0;
//15+15*sin(200*$t);;//angle de pli du chassis


//----------------------------------------------------valeurs déduites des précedentes
//trapeze de l'essieu avant
/*

A________lTrapeze______B
I                                        \
I hTrapeze                        \ HTrapeze
I                                           \
D__________LTrapeze________C


*/
  

lTrapeze=2*(voie/2-((diametreRoueAv/2)+(hTrapeze/2)+dzTrapeze)*sin(AnglePivot));
LTrapeze=2*(voie/2-((diametreRoueAv/2)-(hTrapeze/2)+dzTrapeze)*sin(AnglePivot));
HTrapeze=(hTrapeze/2)*cos(AnglePivot);


///////////////////////////////////////////////////////////////////----------------------------------------------------------------


////////////////tricycle test

rotate(a=[0,gite,0]){
translate(v = [0,0,diametreRoueAv/2])trainAvant();
% translate(v = [0,empatement,diametreRoueAr/2])roueAr();

chassis();

////////////////////////////////////////////////////////////////test carosserie freeship
mirror([0,0,1]){
%
translate([0,1500,-100])
rotate([0,180,90])
scale([25,40,30])
import_stl("test.freeship.stl", convexity = 5);
mirror([1,0,0]){
%
translate([0,1500,-100])
rotate([0,180,90])
scale([25,40,30])
import_stl("test.freeship.stl", convexity = 5);
}
}

%
translate([0,1500,1000])
rotate([0,180,90])
scale([25,40,40])
import_stl("test.freeship.stl", convexity = 5);
mirror([1,0,0]){
%
translate([0,1500,1000])
rotate([0,180,90])
scale([25,40,40])
import_stl("test.freeship.stl", convexity = 5);
}
///////////////////////////////////////////


}






/////////////////////////////////////////////////////////////////////////////////////////////////MODULES




module siege(){/////////////////////////////////////////////////////////////////siege
translate(v = [0,SiegePedalier-250,hSiege]){
rotate(a=[90,0,-90])linear_extrude(file = "siege.dxf", height = 250, center = true);


}

}




module chassis(){/////////////////////////////////////////////////////////////////////////
rotate(a=[cambrure,0,0]){


%siege();

translate(v = [0,0,diametreRoueAv-hTrapeze/2])rotate(a=[90,0,-90]){//centre sur le milieu de l'essieu
linear_extrude(file = "chassis.dxf", height = 30, center = true);

translate(v = [-700,0,0]){

rotate(a=[0,0,180+3.14*cambrure])cube(size = [650,30,3], center = false);
translate(v = [0,0,30])linear_extrude(file = "fourchAr.dxf", height = 30, center = true);
translate(v = [0,0,-30])linear_extrude(file = "fourchAr.dxf", height =30, center = true);
}

}
translate(v = [0,0,diametreRoueAr/2]) pedalier();
}
}


module pedalier(){///////////////////////////////////////////////////////pedalier
translate(v = [0,-250,hPedalier])
rotate(a=[90,0,90])
cylinder(h = 20, r=170,center=true, $fn = qualite);

}/////////////////////////////////////////////////////////////////////////////////fin pedalier





module roueAr(){//////////////////////////////////////////////////////////roueArriere
rotate(a=[90,0,90])
rotate_extrude(convexity = 10, $fn = qualite)
translate([(diametreRoueAr/2)-(RPneuAr/2), 0, 0])
circle(r = RPneuAr, $fn = qualite);

}



