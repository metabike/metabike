/*
Roue avant de tricycle à tambour sturmy archer
*/

include <moyeux.sturmyArcher.scad>;


/*
pour test-----------------------------------------
*/
RPneuAv=25;//rayon pneu Av
diametreRoueAv=540;//16 pouces
//-----------------------------------------------


module RoueAvantSturmyArcher(){
	rotate([0,90,0]){
		color([0.7,0.7,0.7]) 
		MoyeuSturmyArcher();
		
		//jante
		color([0.7,0.7,0.7]) 
		rotate_extrude(convexity = 10, $fn = 100)
		translate([diametreRoueAv/2-RPneuAv/1.5, 0, 0])
		circle(r = RPneuAv*1.1, $fn = 100);
		
		//pneu
		color([0,0,0])
		rotate_extrude(convexity = 10, $fn = 100)
		translate([diametreRoueAv/2, 0, 0])
		circle(r = RPneuAv, $fn = 100);
	
	}
}